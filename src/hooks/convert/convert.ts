import {reactive} from "vue"
import {type UploadFile} from "element-plus"
import {type WasmImageConvert} from "@/types/types"
import { ElMessage } from 'element-plus'
export default function(){
    class Convert{
        uploadStyle:boolean
        outputFormats:{label:string}[]
        config:{outputFormat:string,quality:number,gifMaxColorNum:number,webpLossless:boolean,webpExact:boolean}
        wasmFunc:WasmImageConvert={
            toPNG:(bytes:Uint8Array,fileName:string)=>{return new Uint8Array()},
            toJPEG:(bytes:Uint8Array,fileName:string,quality:number)=>{return new Uint8Array()},
            toGIF:(bytes:Uint8Array,fileName:string,colorNum:number)=>{return new Uint8Array()}
        }
        constructor(){
            this.uploadStyle=false
            this.outputFormats=[
                {label:"png"},{label:"webp"},{label:"jpg"},{label:"gif"},
            ]
            this.config={
                outputFormat:"png",
                quality:100,
                gifMaxColorNum:256,
                webpLossless:true,
                webpExact:true
            }
        }
        //挂载初始化
        onMountedFunc(){
            const go =new window.Go()
            WebAssembly.instantiateStreaming(fetch("util/image.wasm"), go.importObject).then((result) =>{
                go.run(result.instance)

                this.wasmFunc.toPNG=toPNG
                this.wasmFunc.toJPEG=toJPEG
                this.wasmFunc.toGIF=toGIF
            })
        }
        //
        limit(fileType:string,fileSize:number):boolean{
            if(fileSize>1024*1024*20){
                ElMessage.error("文件大小超过20MB")
                return false
            }else if(fileSize<0){
                ElMessage.error("非法文件")
                return false
            }
            let ok=true
            switch(fileType){
                case "image/jpeg":
                    ok=ok&&true
                    break
                case "image/webp":
                    ok=ok&&true
                    break
                case "image/png":
                    ok=ok&&true
                    break
                case "image/gif":
                    ok=ok&&true
                    break
                default:
                    ElMessage.error("不支持该类型文件")
                    return false
            }
              return ok
        }

        //获取输出文件名
        getOutputFileName(fileName:string):string{
            return fileName.substring(0,fileName.lastIndexOf(".")+1)+this.config.outputFormat || "unknown"+this.config.outputFormat
        }
        //下载文件
        downloadFile(name:string,url:string){
            let a = document.createElement('a');
            let event = new MouseEvent('click');
            a.download = name;
            a.href = url;
            a.dispatchEvent(event);
        }
    }
    function initConvert(){
        return reactive(new Convert())
    }
    class Dialog{
        visable=false
        url=""
        ImagePreview(file:UploadFile){
            if(file.url==undefined){
                return
            }
            this.url=file.url
            this.visable=true
        }
    }
    function initDialog(){
        return reactive(new Dialog())
    }
    return {initConvert,initDialog}
}
