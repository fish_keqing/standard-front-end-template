import { createRouter,createWebHashHistory} from "vue-router"
import CONVERT from "@/views/convert/convert.vue"
const router=createRouter({
    history:createWebHashHistory(),
    routes:[
        {
            path:"/convert",
            component:CONVERT,
        },
        {
            path:"/",
            redirect:"/convert"
        }
    ]
})

export default router