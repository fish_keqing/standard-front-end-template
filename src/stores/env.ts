import {defineStore} from "pinia"

export const useEnvStore=defineStore("env",{
    state(){
        return{
            env:JSON.parse(localStorage.getItem("env") as string) || {isCollapse:false}
        }
    }
})