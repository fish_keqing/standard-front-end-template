//api url
export const ReqConvertWebpUrl="/api/convert/webp"

//convert
export type WasmImageConvert={
    toPNG : (bytes:Uint8Array,fileName:string) => Uint8Array;
    toJPEG : (bytes:Uint8Array,fileName:string,quality:number) => Uint8Array;
    toGIF : (bytes:Uint8Array,fileName:string,colorNum:number) => Uint8Array;
}

export type ReqWEBP={
    lossless:boolean
	quality:number
	exact:boolean
}


